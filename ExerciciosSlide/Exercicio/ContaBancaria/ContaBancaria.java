package ContaBancaria;

public abstract class ContaBancaria {
    private String nomeCliente;
    private String numConta;
    private double saldo;

    public ContaBancaria(String nomeCliente, String numConta, double saldo) {
        this.nomeCliente = nomeCliente;
        this.numConta = numConta;
        this.saldo = saldo;
    }

    public void sacar(double quantidade){
        if((saldo - quantidade) < 0) {
            System.out.println("Não é possivel sacar essa quantidade");
        } else {
            saldo = saldo - quantidade;
            System.out.println("Valor sacado: " + quantidade + "\nQuantidade atual de saldo é de : " + saldo);
        }
    }

    public void depositar(double quantidade){
        saldo = saldo + quantidade;
        System.out.println("Saldo atual: " + saldo);
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "ContaBancaria{" +
                "nomeCliente='" + nomeCliente + '\'' +
                ", numConta='" + numConta + '\'' +
                ", saldo=" + saldo +
                '}';
    }
}
