package ContaBancaria;

public class ContaEspecial extends ContaBancaria{
    private double limite;

    public ContaEspecial(String nomeCliente, String numConta, double saldo, double limite) {
        super(nomeCliente, numConta, saldo);
        this.limite = limite;
    }

    public void sacar(double quantidade){
        if((getSaldo() - quantidade) < limite) {
            System.out.println("Não é possivel sacar essa quantidade");
        } else {
            setSaldo(getSaldo() - quantidade);
            System.out.println("Valor sacado: " + quantidade + "\nQuantidade atual de saldo é de : " + getSaldo());
        }
    }
}
