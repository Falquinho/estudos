package ContaBancaria;

public class ContaPoupanca extends ContaBancaria{
    private double diaRendimento;

    public ContaPoupanca(String nomeCliente, String numConta, double saldo, double diaRendimento) {
        super(nomeCliente, numConta, saldo);
        this.diaRendimento = diaRendimento;
    }

    public void calcularNovoSaldo(){
        diaRendimento = getSaldo() * diaRendimento;
        setSaldo(getSaldo() + diaRendimento);
        System.out.println("Saldo atual é: " + getSaldo());
    }
}
