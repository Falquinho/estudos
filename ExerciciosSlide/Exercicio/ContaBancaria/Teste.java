package ContaBancaria;

public class Teste {
    public static void main(String[] args) {
        ContaBancaria contaEspecial = new ContaEspecial("Roberto", "1234-5", 5000, -100);
        ContaBancaria contaPoupanca = new ContaPoupanca("Clovis", "1235-4", 2500, 0.5);

        contaEspecial.sacar(200);
        contaPoupanca.sacar(100);

        System.out.println();
        contaEspecial.depositar(100);
        contaPoupanca.depositar(200);

        System.out.println();
        ((ContaPoupanca) contaPoupanca).calcularNovoSaldo();

        System.out.println();
        System.out.println(contaEspecial.toString());

        System.out.println();
        System.out.println(contaPoupanca.toString());
    }
}
