package Exemplo1;

import java.util.Arrays;

public class Fila {
    private int tamanho;
    private int[] elementos;

    public Fila() { // Construtor setando valores iniciais
        this.tamanho = 0;
        this.elementos = new int[10];
    }

    public void enfileira(int elemento) { // Metodo de enfileiramento na fila
        aumentaCapacidade();
        this.elementos[this.tamanho] = elemento;
        tamanho++;
    }

    public void aumentaCapacidade() { // Método de redimencionar vetor - ok
        if (tamanho == elementos.length) {
            int[] aux = new int[elementos.length * 2];

            for (int i = 0; i < elementos.length; i++) {
                aux[i] = elementos[i];
            }
            elementos = aux;
        }
    }

    public boolean estaVazia() { // Verifica se a fila esta vazia (retornando true ou false) - ok
        if (tamanho == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void espiar() { // Método para verificar quem é o primeiro da fila
        if (estaVazia()) {
            System.out.println("Fila vazia!");
        } else {
            System.out.println("O primeiro da fila é: " + elementos[0]);
        }
    }

    public void desenfileira() { // Método de desinfileiramento da fila
        if(this.estaVazia()){
            System.out.println("Não ha mais elementos para se remover");
        } else {
            for (int i = 0; i < elementos.length - 1; i++) {
                elementos[i] = elementos[i+1];
            }
        }
        tamanho--;
    }

    public int tamanho() { // Método para verificar tamanho da pilha
        return tamanho;
    }

    @Override
    public String toString() {
        return Arrays.toString(elementos);
    }
}
