package Exemplo1;

public class Main {
    public static void main(String[] args) {
        Fila fila = new Fila();

        fila.enfileira(1);
        fila.enfileira(2);
        fila.enfileira(3);
        fila.enfileira(4);
        fila.enfileira(5);

        System.out.println("Fila esta vazia? " + fila.estaVazia());
        System.out.println("Tamanho da fila: " + fila.tamanho());
        fila.espiar();

        System.out.println(fila.toString());

        fila.desenfileira();
        fila.desenfileira();
        fila.desenfileira();
        fila.desenfileira();
        fila.desenfileira();
        fila.desenfileira();

        System.out.println(fila.toString());
    }
}
