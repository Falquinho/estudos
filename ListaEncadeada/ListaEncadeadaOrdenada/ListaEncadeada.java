public class ListaEncadeada {
    private Celula cabeca;
    private int tamanho;

    public ListaEncadeada(){
        cabeca = new Celula();
        tamanho = 0;
    }

    public void insere(int e){ //inserir elemento na lista encadeada
        Celula aux = cabeca;

        while(aux.getProx() != null){
            aux = aux.getProx();
        }

        Celula novo = new Celula();
        novo.setChave(e);
        aux.setProx(novo);

        tamanho++;
    }

    public void ordenaLista(){
        int []vetor = new int [tamanho];
        Celula primeiro = cabeca.getProx();

        //Vetor pega os valores da lista linear
        for(int i = 0; i < tamanho; i++) {
            while(primeiro != null) {
                vetor[i] = primeiro.getChave();
                primeiro = primeiro.getProx();
            }
        }

        //Ordena vetor
        int aux;
        for(int i = 0; i < vetor.length; i++){
            for(int j = 0; j < (vetor.length-1); j++){
                if(vetor[j] < vetor[j+1]){
                    aux = vetor[j];
                    vetor[j] = vetor[j + 1];
                    vetor[j + 1] = aux;
                }
            }
        }

        //Define os valores ordenados a lista encadeada
        for(int i = 0; i < tamanho; i++) {
            while(primeiro != null) {
                primeiro.setChave(vetor[i]);
                primeiro = primeiro.getProx();
            }
        }
    }

    public void print(){
        for (Celula aux = cabeca.getProx(); aux != null; aux = aux.getProx()){
            System.out.printf("%d ", aux.getChave());
        }
        System.out.println();
    }
}
