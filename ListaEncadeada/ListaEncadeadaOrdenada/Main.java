public class Main {
    public static void main(String[] args) {
        ListaEncadeada lista = new ListaEncadeada();

        lista.insere(5);
        lista.insere(10);
        lista.insere(2);
        lista.insere(17);
        lista.insere(21);
        lista.insere(65);
        lista.insere(45);
        lista.insere(52);

        lista.print();

        lista.ordenaLista();

        lista.print();
    }
}
