package Ordenacao;

public class Main {
	public static void main(String[] args) {
		int[] vetor = { 3, 5, 4, 64, 21, 32 };
		int aux;
		boolean controle;

		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + " ");
		}

		System.out.println("");

		// Começo do código de ordenação//
		for (int i = 0; i < vetor.length; i++) {
			controle = true;
			for (int j = 0; j < (vetor.length - 1); j++) {
				if (vetor[j] < vetor[j + 1]) {
					aux = vetor[j];
					vetor[j] = vetor[j + 1];
					vetor[j + 1] = aux;
					controle = false;
				}
			}
			// Fim do código de ordenação//
			if (controle) { // Controle para a ordenação não executar caso ja esteja ordenado
				break;
			}
		}

		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + " ");
		}
	}
}
