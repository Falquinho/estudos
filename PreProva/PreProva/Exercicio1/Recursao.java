package Exercicio1;

public class Recursao {

    public int Recursao(int n) {
        System.out.print(n + " ");
        if (n != 1) {
            if (n % 2 == 0) { // Se for par, só divide
                return 1 + Recursao(n / 2);

            }
            if (n % 2 != 0) { // Se for impar multiplica por 3 e adiciona 1 (para virar par)
                return 1 + Recursao((3 * n) + 1);
            }
        }
        System.out.println();
        return 1;
    }
}
