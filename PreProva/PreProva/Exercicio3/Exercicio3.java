package Exercicio3;

public class Exercicio3 {

    public static void mergeSortInovador(int[] v, int p, int r) {
        int q; //Meio do vetor

        if (p < r - 1) {
            q = (p + r) / 2;
            if (v.length > 8) {
                mergeSortInovador(v, p, q);
            }
            ordena(v);

            if (v.length > 8) {
                mergeSortInovador(v, q, r);
            }

            ordena(v);
            intercala(v, p, q, r);
        }
    }

    private static void ordena(int[] v) {
        int n = v.length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (v[j - 1] > v[j]) {
                    temp = v[j - 1];
                    v[j - 1] = v[j];
                    v[j] = temp;
                }

            }
        }
    }

    public static void intercala(int v[], int p, int q, int r) {
        int i, j, k, w[] = new int[v.length];
        i = p;
        j = q;
        k = 0;
        while (i < q && j < r) {
            if (v[i] < v[j]) {
                w[k] = v[i];
                i++;
            } else {
                w[k] = v[j];
                j++;
            }
            k++;
        }
        while (i < q) {
            w[k] = v[i];
            i++;
            k++;
        }
        while (j < r) {
            w[k] = v[j];
            j++;
            k++;
        }
        for (i = p; i < r; i++)
            v[i] = w[i - p];
    }

    public static void print(int v[]) {
        for (int i = 0; i < v.length; i++) {
            System.out.print(v[i] + " ");
        }
    }
}
