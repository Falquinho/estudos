package Exercicio5;

import java.util.*;

public class Cpf {
    Scanner teclado = new Scanner(System.in);
    String nome, dataNascimento, endereco, cpf; // Variaveis de teclado e atribuição no map
    String pessoa = "";

    // Map de pessoas
    Map<String, Pessoa> map = new HashMap<String, Pessoa>();
    Set<String> set = new HashSet<>();

    public void cadastro() { // Funcionando
        System.out.println("Digite o cpf");
        cpf = teclado.nextLine(); // Teclado

        if (!map.containsKey(cpf)) {
            System.out.println("Digite o nome");
            nome = teclado.nextLine(); // Teclado

            System.out.println("Digite a data de nascimento");
            dataNascimento = teclado.nextLine(); // Teclado

            System.out.println("Digite o endereco");
            endereco = teclado.nextLine(); // Teclado

            Pessoa pessoa = new Pessoa(nome, dataNascimento, endereco);
            map.put(cpf, pessoa);

            System.out.println("Pessoa cadastrada com sucesso!\n" + map.get(cpf) + "\n");
        } else {
            set.add(cpf);
            System.out.println("CPF ja cadastrado! \n");
        }
    }

    public void buscarDados() {
        System.out.println("Digite o CPF da pessoa a ser buscada: ");
        String busca = teclado.nextLine(); // Teclado

        if (map.containsKey(busca)) {
            System.out.println(map.get(busca));
        } else {
            System.out.println("Pessoa não encontrada \n");
        }
    }

    public void atualizarDados() { // Funcionando
        System.out.println("Digite o CPF da pessoa a ser alterada: ");
        String cpf = teclado.nextLine(); // Teclado

        if (map.containsKey(cpf)) {
            System.out.println("Digite o nome");
            nome = teclado.nextLine();

            System.out.println("Digite a data de nascimento");
            dataNascimento = teclado.nextLine();

            System.out.println("Digite o endereco");
            endereco = teclado.nextLine();

            // Atribuição de valores
            Pessoa pessoa = map.get(cpf);
            pessoa.setNome(nome);
            pessoa.setDataNascimento(dataNascimento);
            pessoa.setEndereco(endereco);

        } else {
            System.out.println("Pessoa não encontrada\n");
        }
    }

    public void impressaoAtualiza() {
        System.out.println("Pessoa atualizada com sucesso!\n" + map.get(cpf) + "\n");
    }

    public void pessoaDuplicada(){
        Iterator<String> duplicado = set.iterator();
        
        if (set.isEmpty()){ //Verifica se set esta vazio
            System.out.println("Nenhum CPF duplicado");
        } else { //Caso não esteja, printa todos os CPFs repetidos
            while(duplicado.hasNext()){
                System.out.println(duplicado.next());
            }
        }
    }
}
