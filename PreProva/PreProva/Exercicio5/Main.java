package Exercicio5;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Cpf cpf = new Cpf();
        Scanner teclado = new Scanner(System.in);
        int opcao;

        do {
            System.out.println("Escolha uma das opções:");
            System.out.println("(1) Cadastrar um contribuinte");
            System.out.println("(2) Buscar informações");
            System.out.println("(3) Atualizar dados do contribuinte");
            System.out.println("(4) Listar que teve tentiva de duplicar o CPF");
            System.out.println("(0) Para finalizar");
            opcao = Integer.parseInt(teclado.nextLine());

            switch (opcao) {
                case 1: // Casdastrar pessoa
                    cpf.cadastro();
                    break;

                case 2: // Buscar informação de pessoa
                    cpf.buscarDados();
                    break;

                case 3: // Atualização dos dados
                    cpf.atualizarDados();
                    cpf.impressaoAtualiza();
                    break;

                case 4: // Lista quantidade de vezes que o CPF foi duplicado
                    cpf.pessoaDuplicada();
                    break;
            }
        } while (opcao != 0);
        // Finaliza programa
        teclado.close();
    }
}
