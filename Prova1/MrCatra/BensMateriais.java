public abstract class BensMateriais {
    private String anoDaCompra;
    private String estadoItem;

    public BensMateriais(String anoDaCompra, String estadoItem) {
        this.anoDaCompra = anoDaCompra;
        this.estadoItem = estadoItem;
    }

    public String getAnoDaCompra() {
        return anoDaCompra;
    }

    public void setAnoDaCompra(String anoDaCompra) {
        this.anoDaCompra = anoDaCompra;
    }

    public String getEstadoItem() {
        return estadoItem;
    }

    public void setEstadoItem(String estadoItem) {
        this.estadoItem = estadoItem;
    }
}
