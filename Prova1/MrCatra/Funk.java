import java.util.Scanner;

public class Funk extends Musicas implements Valores{
    Scanner entrada = new Scanner(System.in);
    private String estilo;
    String[] parceiros;

    public Funk(String nome, String dataDeComposicao, int quantidadeDeVezesTocada, String estilo) {
        super(nome, dataDeComposicao, quantidadeDeVezesTocada);
        this.estilo = estilo;
    }

    public String[] listarParceiros(int quantidade){
        parceiros = new String[quantidade];

        if (quantidade > 10){
            parceiros[0] = "Não é possivel adicionar mais que 10 parceiros";
            return parceiros;
        } else {
            for (int i = 0; i < parceiros.length; i++) {
                System.out.println("Digite o nome do parceiro " + (i+1));
                parceiros[i] = entrada.nextLine();
            }
            return parceiros;
        }
    }

    @Override
    public double calculaValor() {
        double valor = 0;
        int cont = 0;

        if(estilo.equalsIgnoreCase("proibidão")){
            valor = 1.000;
        } else {
            for (int i = 0; i < parceiros.length; i++) {
                if(parceiros[i].equalsIgnoreCase("Valesca Popozuda")){
                    valor += 100.000;
                } else {
                    cont++;
                }
            }
            valor += (50.000 * cont);
        }
        return valor;
    }
}
