public class Joias extends BensMateriais implements Valores{
    private String quilates;
    private String tipoJoia;

    // tipo da jóia (pulseira ou brinco)
    public Joias(String anoDaCompra, String estadoItem, String quilates, String tipoJoia) {
        super(anoDaCompra, estadoItem);
        this.quilates = quilates;
        this.tipoJoia = tipoJoia;
    }

    @Override
    public void calculaValor() {

    }
}
