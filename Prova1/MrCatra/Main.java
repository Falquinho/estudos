public class Main {

    public static void main(String[] args) {
        /**
         * - Funk, Rap, POP
         * - Nome, Data da composição (dia/mes/ano) e Quantidade de vezes que já foi tocada nas rádios
         *
         * - Funk: para cada faixa nesse estilo armazenar uma listagem contendo até 10 nomes dos parceiros de composição e se a faixa é do estilo "proibidão".
         * - Pop: armazenar quantas vezes a faixa já foi tocada no Spotify e a quantidade de processos de direitos autorais vinculados à faixa
         * - Rap: armazenar qual o estilo do Rap (freestyle, flow ou gangsta) e uma listagem de até 5 nomes dos países nos quais a faixa foi tocada.
         */
    }
}
