public abstract class Musicas {
    private String nome;
    private String dataDeComposicao;
    private int quantidadeDeVezesTocada;

    public Musicas(String nome, String dataDeComposicao, int quantidadeDeVezesTocada) {
        this.nome = nome;
        this.dataDeComposicao = dataDeComposicao;
        this.quantidadeDeVezesTocada = quantidadeDeVezesTocada;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataDeComposicao() {
        return dataDeComposicao;
    }

    public void setDataDeComposicao(String dataDeComposicao) {
        this.dataDeComposicao = dataDeComposicao;
    }

    public int getQuantidadeDeVezesTocada() {
        return quantidadeDeVezesTocada;
    }

    public void setQuantidadeDeVezesTocada(int quantidadeDeVezesTocada) {
        this.quantidadeDeVezesTocada = quantidadeDeVezesTocada;
    }
}
