public class Relogios extends BensMateriais implements Valores{
    private String tipoRelogio;
    private String marca;

    public Relogios(String anoDaCompra, String estadoItem, String tipoRelogio, String marca) {
        super(anoDaCompra, estadoItem);
        this.tipoRelogio = tipoRelogio;
        this.marca = marca;
    }

    @Override
    public void calculaValor() {

    }
}
