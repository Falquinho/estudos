package Exercicio2;

public class ListaEncadeada {
    private No cabeca;
//    private int tamanho;

    public ListaEncadeada() {
        this.cabeca = new No();
    }

    public void insereInicio(int elemento) {
        No novo = new No();
        No aux = cabeca;
        novo.setChave(elemento);

        while (aux != null) {
            aux = aux.getProx();
            novo.setProx(aux);
            novo = novo.getProx();
        }
        aux = novo;
    }

    public void insereFinal(int elemento) {
        No aux = cabeca;

        while (aux.getProx() != null) {
            aux = aux.getProx();
        }

        No novo = new No();
        novo.setChave(elemento);
        aux.setProx(novo);
    }

    public void removerElemento() {
        No aux = cabeca;

        if (aux.getProx() == null) { // Caso a lista esteja sem elementos
            System.out.println("Nenhum elemento a ser removido");

        } else {
            try {
                for (; aux != null; aux = aux.getProx()) { // Remove elemento subsequentes a primeiro
                    if (aux.getProx().getProx() == null) {
                        aux.setProx(null);
                    }
                }
            } catch (NullPointerException e) {
                if (aux.getProx().getProx() == null) { // Remover primeiro elemento
                    aux.setProx(null);
                    System.out.println("Todas os objetos removidos");
                }
            }
        }
    }

    public void imprimirEsquerdaDireita() {
        for (No aux = cabeca.getProx(); aux != null; aux = aux.getProx()) {
            System.out.printf("%d ", aux.getChave());
        }
    }

    public void imprimirDireitaEsquerda() {

    }
}
