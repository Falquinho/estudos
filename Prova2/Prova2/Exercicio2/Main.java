package Exercicio2;

public class Main {
    public static void main(String[] args) {
        ListaEncadeada lista = new ListaEncadeada();

        lista.insereFinal(4);
        lista.insereFinal(8);
        lista.insereInicio(1);

        lista.imprimirEsquerdaDireita();
    }
}
