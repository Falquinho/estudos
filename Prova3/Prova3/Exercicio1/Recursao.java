package Exercicio1;

public class Recursao {
    public int piso(float n){
        if (n <= 1)
            return 0;
        else {
            return piso(n/3) + 1;
        }
    }
}
