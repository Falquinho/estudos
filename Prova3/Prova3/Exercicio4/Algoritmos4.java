package Exercicio4;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Algoritmos4 {

	// Retorna um ArrayList contendo todas as linhas do arquivo
	public static ArrayList<String> leViagens(String urlArquivo) {
		BufferedReader bf = null;
		ArrayList<String> viagens = new ArrayList<String>();

		try {
			bf = new BufferedReader(new FileReader(new File(urlArquivo)));

			while(bf.ready())
				viagens.add(bf.readLine());

			bf.close();	
		}
		catch(FileNotFoundException e) {
			System.out.println("FileNotFoundException!");
			e.printStackTrace();
			System.exit(1);
		}
		catch(IOException e) {
			System.out.println("IOException!");
			e.printStackTrace();
			System.exit(1);
		}

		return viagens;
	}

}