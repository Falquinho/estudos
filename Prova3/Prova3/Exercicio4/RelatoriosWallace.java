package Exercicio4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class RelatoriosWallace {

	public void Relatorio1(String url) {
		ArrayList<String> lista = Algoritmos4.leViagens(url);
		Set<String> cidade = new HashSet<>();
		Iterator ler = lista.iterator();
		
		while(ler.hasNext()) {
			String palavra = (String) ler.next();
			String[] texto = palavra.split(",");
			
			cidade.add(texto[0]);
		}
		
		Iterator imprime = cidade.iterator();
		while(imprime.hasNext()) {
			System.out.println(imprime.next());
		}
	}
	
	public void Relatorio2(String url) {
		ArrayList<String> lista = Algoritmos4.leViagens(url);
		Map<String, Double> valores = new HashMap<>();
		Iterator ler = lista.iterator();
		
		while(ler.hasNext()) {
			String palavra = (String) ler.next();
			String[] texto = palavra.split(",");
			
			if(valores.containsKey(texto[0])) {
				double valorAntigo = valores.get(texto[0]);
				valores.put(texto[0], valorAntigo + Double.parseDouble(texto[3]));
			} else {
				valores.put(texto[0], Double.parseDouble(texto[3]));
			}
		}
		
		System.out.println(valores);
	}
}
