package Exercicio1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		int n = entrada.nextInt();
		float[] v = new float [n];

		for (int i = 0; i < v.length; i++) {
			v[i] = entrada.nextFloat();
		}
		System.out.println(soma(v, v.length));
	}

	public static float soma(float[] v, int n) {
		if(v[n - 1] == v[0]) {
			return v[0];
		} else {
			return v[n - 1] + soma(v, n-1);
		}
	}
}
