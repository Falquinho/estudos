package Recursao.Heap;

public class Heap {

	public int pai(int i) { // O pai do vetor
		if (i == 0) {
			return 0;
		} else {
			return (i - 1) / 2;
		}
	}

	public int esquerda(int i) {
		return 2 * i + 1;
		// ou "return 2 * (i + 1) - 1"
	}

	public int direita(int i) {
		return 2 * i + 2;
		// ou "return 2 * (i + 1) + 1" ou esqueda(i) + 1
	}

	public void desce(int n, int S[], int i) { // Metodo parar andar no vetor e definir ele como heap
		int e, d, maior;

		e = esquerda(i);
		d = direita(i);

		if (e < n && S[e] > S[i]) {
			maior = e;
		} else {
			maior = 1;
		}
		if (d < n && S[d] > S[maior]) {
			maior = d;
		}
		if (maior != i) {
			troca(S[i], S[maior]);
			desce(n, S, maior);
		}
	}

	public void troca(int i, int j) {

	}

	public void heap(int S[], int n) {
		for (int i = n - 1; i >= 0; i--) {
			desce(n, S, i);
		}
	}
}
