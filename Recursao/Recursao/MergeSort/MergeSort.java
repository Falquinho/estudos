package Recursao.MergeSort;

public class MergeSort {
	public void intercala(int v[], int p, int q, int r) {
		int aux[] = new int[r - p]; // Vetor para auxilixar na troca
		int i = p, j = q, k = 0;

		while (i < q && j < r) {
			if (v[i] < v[j]) { // Pega o vetor da "esquerda"
				aux[k] = v[i];
				i++;
			} else {
				aux[k] = v[j]; // Pega o vetor da "direita"
				j++;
			}
			k++;
		}

		// Temos a certeza que uma das metades acabou
		while (i < q) { // Verifica se o lado eaquerdo ainda contem elementos
			aux[k] = v[i];
			k++;
			i++;
		}

		while (j < r) { // Verifica se o lado direito ainda contem elementos
			aux[k] = v[j];
			k++;
			j++;
		}

		// Copia o vetor "v" original
		// i = marca o vetor inicial / k = marca o vetor auxiliar
		for (i = p, k = 0; i < r; i++, k++) {
			v[i] = aux[k];
		}
	}
}
