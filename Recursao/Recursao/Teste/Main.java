package Recursao.Teste;

public class Main {
	public static void main(String[] args) {
		int x = 5, n = 3; 
		System.out.println(pot(x, n));
		
	}
	
	public static int pot(int x, int n) {
		if (n == 0) { //Condição e parada
			return 1;
		} else { //passo recursivo
			return x * pot(x, n-1);
		}
	}
}
