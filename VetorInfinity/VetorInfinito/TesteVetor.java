import java.util.Scanner;

public class TesteVetor {
    public static void main(String[] args) {
        VetorInfinito v = new VetorInfinito();
        Scanner scanner = new Scanner(System.in);

        int opcao;
        System.out.println("Digite o que você quer fazer:");
        System.out.println("(1) Inserir elemento em uma posicao");
        System.out.println("(2) Remover");
        System.out.println("(3) Retorna elemento");
        System.out.println("(0) Finalizar");

        // Faz leituras
        do {
            System.out.print("Opção: ");
            opcao = scanner.nextInt();

            int elemento = 0;
            int posicao = 0;

            switch (opcao) {
                case 1:
                    elemento = scanner.nextInt();
                    posicao = scanner.nextInt();
                    v.adicionaElemento(posicao, elemento);
                    break;
                case 2:
                    posicao = scanner.nextInt();
                    v.remove(posicao);
                    break;
                case 3:
                    posicao = scanner.nextInt();
                    v.retornaElemento(posicao);
                    break;
                default:
                    System.out.println("Opcao inválida");
                    break;
            }


        } while (opcao != 0);

    }
}