public class VetorInfinito {
    private int[] vetor;

    public VetorInfinito() {
        vetor = new int[50];
    }

    public void adicionaElemento(int posicao, int elemento) {
        if (posicao < vetor.length) {
            vetor[posicao] = elemento;
        } else {
            redimensionaVetor(posicao + 1);
            vetor[posicao] = elemento;
        }
    }

    public void retornaElemento(Integer posicao) {
        if (posicao < vetor.length) {
            System.out.println(vetor[posicao]);
        } else {

            System.out.println("Posição inválida");
        }
    }

    public void remove(int posicao) {
        vetor[posicao] = 0;
    }

    private void redimensionaVetor(int novoTamanho) {

        int vetorAux[] = new int[novoTamanho];

        for (int i = 0; i < vetor.length; i++) {
            vetorAux[i] = vetor[i];
        }

        vetor = vetorAux;

    }

}